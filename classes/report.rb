#Класс для создание отчета
class Report

    attr_accessor :data, :sort, :filter

    #Создание отчета
    def initialize(data)
        @data = data
    end

    #Сортировка отчета
    def sort(by = false, order = false)
        if (by and order)
            
            @sort = {by => order}

            if order == 'asc'
                @data = @data.sort_by{|item|item[by]}
            elsif order == 'desc'
                @data = @data.sort_by{|item|item[by]}.reverse
            end
            
        end
    end

    #фильтр отчета
    def filter(field = false, value = false)
        if (field and value)

            @filter = {field => value}

            @data = @data.select {|item| item[field].include?(value)}
        end
    end

    #Выгрузка отчета
    def formatHtml()
        
        html = "<h3>Отчет по данным</h3>\n"

        for item in @data 
            html += "<p>\n"
            item.each {|key, value| html += "#{key}: #{value}<br>\n" }
            html += "</p>\n\n"
        end
        
        return html
    end

end