#Этот модуль отвечает за загрузку данных с какого-нибудь сервиса. 
#Сейчас сделана заглушка и данные берутся с файла data.json

require 'json'

module Rest

    #Получает массив данных
    def get (path = false)
        begin
            File.open(path, "r") do |file|
                return Rest.convertJsonToArray(file.read())
            end
        rescue 
            puts "Ошибка в попытке открыть файл"
        end
        return false

    end

    #Преобразует строку JSON в Массив
    def convertJsonToArray (string)
        begin
            return JSON.parse(string)
        rescue
            puts "Ошибка в попытке преобразовать из JSON"
        end
        return false
    end
end