require_relative "modules/rest.rb"
require_relative "modules/sender.rb"
require_relative "classes/report.rb"

include Rest
include Sender

#Загрузка данных из какого-то источника
data = Rest.get('tmp/data.json')

# Генерируем отчет
report = Report.new(data)

# Сортировка данных
report.sort('code', 'asc') # asc || desc

# Фильтр данных 
report.filter('guest', '@email.com')

# Отправляем отчет на E-mail
Sender.sendToEmail('example@mail.ru', 'server@domain.ru', 'Report Data', report.formatHtml())
